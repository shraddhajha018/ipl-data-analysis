# IPL Data Analysis

The Python project on IPL (Indian Premier League) data analysis using the Pandas library aims to explore and analyze various aspects of IPL matches and players' performance.

Leveraging the powerful capabilities of Pandas, the project involves data manipulation, cleaning, aggregation, and visualization to extract meaningful insights from the IPL dataset.

In this project we are going to analyse the dataset using Python Pandas on Windows/Linux/MacOS.
Besides pandas we also used matplotlib python module for visualization of this dataset.
 
The whole project is divided into four major parts ie reading, analysis, visualization and export.
All these part are further divided into menus for easy navigation.

The path of the CSV file needs to be updated in the .py file according to the location of the code repository you downloaded in your system.
